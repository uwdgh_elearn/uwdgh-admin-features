<?php

if ( !class_exists( 'UWDGH_ThemeUpdateNotification' ) ) {

  if ( ! function_exists( 'wp_get_themes' ) ) {
    require_once ABSPATH . 'wp-admin/includes/theme.php';
  }

  class UWDGH_ThemeUpdateNotification {

    static $option_name_prefix;

    function __construct() {

      self::$option_name_prefix = UWDGH_AdminFeatures_AFFIX.'_update_notification_disabled_theme_';

      // register setting options
      self::register_plugin_settings();

      // add filter to hook
      add_filter( 'site_transient_update_themes', array( __CLASS__, 'disable_theme_notifications' ), 10, 1 );

    }

    /**
    * Theme update notifications tab
    */
    static function uwdgh_admin_features_tab_theme_update_notifications() {
    	global $uwdgh_admin_features_active_tab; ?>
    	<a class="nav-tab <?php echo $uwdgh_admin_features_active_tab == 'theme-update-notification' || '' ? 'nav-tab-active' : ''; ?>" href="<?php echo admin_url( 'options-general.php?page=uwdgh-admin-features&tab=theme-update-notification' ); ?>"><?php _e( 'Theme update notifications', 'uwdgh-admin-features' ); ?> </a>
    	<?php
    }

    /**
    * Theme update notifications content
    */
    static function uwdgh_admin_features_options_page_theme_update_notifications() {
      global $uwdgh_admin_features_active_tab;
      if ( '' || 'theme-update-notification' != $uwdgh_admin_features_active_tab )
        return;
      ?>
      <h3><?php _e('Theme update notifications','uwdgh-admin-features');?></h3>
      <p><?php _e('Turn off theme update notification for any installed theme on this site.','uwdgh-admin-features');?></p>
      <form action="options.php" method="post" id="uwdgh-admin-features-options-form">
        <?php settings_fields(UWDGH_AdminFeatures_AFFIX.'_options_theme_update_notification'); ?>
        <table class="wp-list-table widefat plugins">
          <thead>
            <tr>
              <td></td>
              <th scope="col" id="name" class="manage-column column-name column-primary"><?php _e('Theme','uwdgh-admin-features');?></th>
              <th scope="col" id="description" class="manage-column column-description"><?php _e('Description','uwdgh-admin-features');?></th>
            </tr>
          </thead>
          <?php
          $Active_theme = wp_get_theme();

          $all_themes = wp_get_themes();
          foreach ($all_themes as $key => $Theme) {
            $option_name = self::construct_option_name($key);
            $active = "inactive";
            if ( $Theme->Name == $Active_theme->Name ) {
              $active = "active";
            }
          ?>
            <tr class="<?php echo $active; ?>" valign="top">
              <th class="check-column">
                <input type="checkbox" id="<?php echo $option_name; ?>" name="<?php echo $option_name; ?>"  value="1" <?php checked(1, get_option($option_name), true); ?> />
              </th>
              <td scope="row" class="plugin-title column-primary">
                <label for="<?php echo $option_name; ?>" title="<?php echo $option_name; ?>">
                  <strong><?php echo _( $Theme->Name );?></strong>
                </label>
                <img src="<?php echo $Theme->get_screenshot() ?>" />
              </td>
              <td scope="row" class="column-description">
                <div class="plugin-description"><p><?php echo _( $Theme->Description );?></p></div>
                <div class="plugin-version-author-uri"><p><?php _e('Version','uwdgh-admin-features');?> <?php echo _( $Theme->Version );?></p></div>
                <?php if (get_option($option_name)) { ?>
                  <div class="notice inline notice-warning notice-alt"><p><?php _e('Update notifications disabled.','uwdgh-admin-features');?></p></div>
                <?php } ?>
              </td>
            </tr>
          <?php
          }
          ?>
        </table>
        <?php submit_button(); ?>
      </form>
      <?php
    }

    /**
    * Register options for all themes
    */
    private static function register_plugin_settings() {
      $all_themes = wp_get_themes();
      foreach ($all_themes as $key => $Theme) {
        register_setting(UWDGH_AdminFeatures_AFFIX.'_options_theme_update_notification',self::construct_option_name($key), array('default' => 0,));
      }
    }

    /**
    * helper function
    */
    private static function construct_option_name( $key ) {
      if ($key) {
        $option_name = self::$option_name_prefix . $key;
        return $option_name;
      } else {
        throw new \Exception("No option name key provided", 1);
      }
    }

    /**
    * Disable theme notification
    */
    static function disable_theme_notifications( $value ) {
      if ( isset( $value ) && is_object( $value ) ) {
        $all_themes = wp_get_themes();
        foreach ($all_themes as $key => $Theme) {
          if ( get_option(self::construct_option_name($key)) ) {
            unset( $value->response[$key] );
          } else {
            update_option(self::construct_option_name($key), 0);
          }
        }
      }
      return $value;
    }

    /**
    * Dispose plugin options
    */
    private static function delete_feature_options() {
      $all_themes = wp_get_themes();
      foreach ($all_themes as $key => $Theme) {
        delete_option(self::construct_option_name($key));
      }
    }

    /**
    * Dispose plugin option upon plugin deactivation
    */
    static function uwdgh_admin_features_deactivate() {
      // nothing to do here
    }

    /**
    * Dispose plugin option upon plugin deletion
    */
    static function uwdgh_admin_features_uninstall() {
      self::delete_feature_options();
    }

  }

  New UWDGH_ThemeUpdateNotification;

}
