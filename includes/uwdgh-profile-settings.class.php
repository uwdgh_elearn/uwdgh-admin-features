<?php

if ( !class_exists( 'UWDGH_ProfileSettings' ) ) {

  class UWDGH_ProfileSettings {
		
    function __construct() {
			
			/**
			 * implement hook admin_init
			 */
			add_action('admin_init', array( __CLASS__, 'uwdgh_profile_settings_register_settings' ) );

			/**
			 * implement hook init
			 */
			add_action('init', array( __CLASS__, 'uwdgh_profile_settings_disable_application_passwords' ), 11 );

		}
		
    /**
    * Admin columns tab
    */
    static function uwdgh_admin_features_tab_profile_settings() {
    	global $uwdgh_admin_features_active_tab; ?>
    	<a class="nav-tab <?php echo $uwdgh_admin_features_active_tab == 'profile-settings' || '' ? 'nav-tab-active' : ''; ?>" href="<?php echo admin_url( 'options-general.php?page=uwdgh-admin-features&tab=profile-settings' ); ?>"><?php _e( 'Profile settings', 'uwdgh-admin-features' ); ?> </a>
    	<?php
    }
		
    /**
    * Profiles settings page
    */
    static function uwdgh_admin_features_options_page_profile_settings() {
      global $uwdgh_admin_features_active_tab;
      if ( '' || 'profile-settings' != $uwdgh_admin_features_active_tab )
        return;
      ?>
      <h3><?php _e('Profile settings','uwdgh-admin-features');?></h3>
      <form action="options.php" method="post" id="uwdgh-admin-features-options-form">
        <?php settings_fields(UWDGH_AdminFeatures_AFFIX.'_options_profile_settings'); ?>
        <table class="form-table">
          <tr class="even" valign="top">
            <th scope="row">
              <label for="uwdgh_admin_features_disable_application_passwords">
                <?php _e('Disable the Application Passwords Section on the Profile page','uwdgh-admin-features');?>
              </label>
            </th>
            <td>
              <input type="checkbox" id="uwdgh_admin_features_disable_application_passwords" name="uwdgh_admin_features_disable_application_passwords"  value="1" <?php checked(1, get_option(UWDGH_AdminFeatures_AFFIX.'_disable_application_passwords'), true); ?> />
              <span><em>(<?php _e('Default: unchecked','uwdgh-admin-features');?>)</em></span>
              <p class="description"><?php _e('When checked, only users with the Administrator role or "manage_options" capability have the Application Passwords Section enabled on their user Profile Page. The section is disabled for all other users.','uwdgh-admin-features');?>
              </p>
            </td>
          </tr>
        </table>
        <?php submit_button(); ?>
      </form>
    <?php 
		}

		/**
		 * Callback for hook admin_init
		 * Register plugin settings
		 */
		static function uwdgh_profile_settings_register_settings() {
			
			// Option to disable Application Passwords section from Profile page
			register_setting(
				UWDGH_AdminFeatures_AFFIX.'_options_profile_settings',		//settings group name
				UWDGH_AdminFeatures_AFFIX.'_disable_application_passwords',		//name of an option to sanitize and save
				array('default' => 0,)		//Data used to describe the setting when registered
			);

		}

		/**
		 * Callback for hook init
		 * Disables the use of the Application Passwords section from Profile page (except admin roles)
		 * Priority 11 (default + 1)
		 */
		static function uwdgh_profile_settings_disable_application_passwords() {

			$user = wp_get_current_user();
			if ( get_option(UWDGH_AdminFeatures_AFFIX.'_disable_application_passwords') && 
			(!in_array( 'administrator', (array) $user->roles ) || !current_user_can('manage_options')) ) {

				add_filter('wp_is_application_passwords_available', '__return_false');

			}

		}
		
		/**
    * Add options on activation
    */
    static function uwdgh_admin_features_activate() {
      add_option(UWDGH_AdminFeatures_AFFIX.'_disable_application_passwords', 0);
    }

		/**
    * Dispose plugin option upon plugin deactivation
    */
    static function uwdgh_admin_features_deactivate() {
      update_option(UWDGH_AdminFeatures_AFFIX.'_disable_application_passwords', 0);
    }

    /**
    * Dispose plugin option upon plugin deletion
    */
    static function uwdgh_admin_features_uninstall() {
      // remove options
      delete_option(UWDGH_AdminFeatures_AFFIX.'_disable_application_passwords');
    }

	}

  New UWDGH_ProfileSettings;

}