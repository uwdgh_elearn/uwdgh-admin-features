/**
 * jQuery should be accessed through $ by passing the jQuery object into an anonymous function.
 * @see {@link https://developer.wordpress.org/coding-standards/wordpress-coding-standards/javascript/#common-libraries}
 */
(function ($, window, document, undefined) {
	// At the top of the file, set "wp" to its existing value (if present)
	window.wp = window.wp || {};

	// The document ready event executes when the HTML-Document is loaded
	// and the DOM is ready.
	jQuery(document).ready(function( $ ) {

		// remove any existing alert containers
		$('.uwdgh-alert').remove();

		$.ajax({
            type: "POST",
            url: UWDGH_Alerts.admin_ajax_url,
            data: {
				action: 'uwdgh_alert_get' // This is the PHP function to call - note it must be hooked to AJAX
            },
			dataType: 'json',
            success: function ( response, textStatus, jqXHR ) {
				if( response != 0 ){
					// output the alert text to the console
					// console.info( textStatus );
					// console.info( response.alert_text );
					
					// create close button
					let _x = document.createElement('span');
					_x.setAttribute('aria-hidden', 'true');
					_x.innerHTML = '&times;';
					let _close = document.createElement('button');
					_close.setAttribute('type', 'button');
					_close.className = 'close';
					_close.setAttribute('title', 'Close');
					_close.setAttribute('aria-label', 'Close');
					_close.appendChild(_x);
					// create alert
					let _alert = document.createElement('div');
					_alert.className = 'alert';
					_alert.classList.add('alert-'+response.alert_type);
					_alert.innerHTML = response.alert_text;
					_alert.appendChild(_close);
					// create container
					let _alert_container = document.createElement('div');
					_alert_container.className = 'uwdgh-alert';
					_alert_container.classList.add('alert-'+response.alert_type);
					switch (response.alert_type) {
						case 'danger':
							_alert_container.setAttribute('role', 'alert');
							_alert_container.setAttribute('aria-live', 'assertive');
							break;	
						case 'warning':
							_alert_container.setAttribute('role', 'alert');
							_alert_container.setAttribute('aria-live', 'assertive');
							break;				
						default:
							_alert_container.setAttribute('role', 'status');
							_alert_container.setAttribute('aria-live', 'polite');
							break;
					}
					// prevent alert content to be shown as a snippet in search result by adding the data-nosnippet boolean attribute
					// @see @link https://developers.google.com/search/docs/crawling-indexing/robots-meta-tag#data-nosnippet-attr
					_alert_container.setAttribute('data-nosnippet', 'data-nosnippet');
					// append alert to container
					_alert_container.appendChild(_alert);
					// prepend container to body
					document.body.prepend(_alert_container);
				}
            },
			error: function( jqXHR, textStatus, errorThrown ) {
				console.info( textStatus );
				console.error( errorThrown );
			}
        });
		
		$(document).on("click", ".uwdgh-alert .alert button.close", function(e){
			e.preventDefault();
			$('.uwdgh-alert')
				// .attr("aria-hidden", true)
				// .fadeOut("slow");
				.attr("style", "display: none;")
				.addClass("hide")
				.prop("hidden", true)
				.attr("aria-hidden", true)
				.attr("role", "none")
				.attr("aria-live", "off");
		});

	})
	
  // The window load event executes after the document ready event,
  // when the complete page is fully loaded.
  $(window).on('load', function () {
		// nothing to do here
  });


})(jQuery, this, this.document);