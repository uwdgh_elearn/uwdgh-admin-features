<?php

if ( !class_exists( 'UWDGH_Alerts' ) ) {

  class UWDGH_Alerts {

		function __construct() {
			
      // register settings
			add_action( 'admin_init',  array( __CLASS__, 'uwdgh_alerts_register_settings' ) );
			
			// add ajax callback handler
			add_action( 'wp_ajax_uwdgh_alert_get', array( __CLASS__, 'uwdgh_alert_get' ) );
			add_action( 'wp_ajax_nopriv_uwdgh_alert_get', array( __CLASS__, 'uwdgh_alert_get' ) );
			
      // add front-end js
      add_action('wp_enqueue_scripts', array( __CLASS__, 'uwdgh_alert_enqueue_script' ));

			// admin info notice
			if ( get_option(UWDGH_AdminFeatures_AFFIX.'_enable_alert') ) {
				// admin notice
				add_action( 'admin_notices',  array( __CLASS__, 'alert_banner_admin_notice') );
			}

			// add menu item
			add_action('admin_menu', array( __CLASS__, 'uwdgh_alerts_menu_item' ));
			// add admin bar item
			add_action('admin_bar_menu', array( __CLASS__, 'uwdgh_alerts_admin_bar_item' ), 99);

			// add capability hook
			add_filter( 'option_page_capability_'.UWDGH_AdminFeatures_AFFIX.'_options_alerts', array( __CLASS__, 'uwdgh_alerts_option_page_capability' ) );

		}

		/**
    * Register option settings
    */
		static function uwdgh_alerts_register_settings() {
      register_setting(UWDGH_AdminFeatures_AFFIX.'_options_alerts',UWDGH_AdminFeatures_AFFIX.'_enable_alert', array('default' => 0,));
      register_setting(UWDGH_AdminFeatures_AFFIX.'_options_alerts',UWDGH_AdminFeatures_AFFIX.'_alert_text', array('sanitize_callback' => array( __CLASS__, 'uwdgh_alert_sanitize_textarea_field' ) ,'default' => '',));
      register_setting(UWDGH_AdminFeatures_AFFIX.'_options_alerts',UWDGH_AdminFeatures_AFFIX.'_alert_type', array('default' => '',));
		}

		/**
    * Return capability for this feature.
		* Normally, the 'manage_options' capability is required to update options,
		* but for this feature we want to allow a user with capability 'edit_theme_options'
    */
		static function uwdgh_alerts_option_page_capability( $capability ) {
			return 'edit_theme_options';
		}

		/**
    * Add alert page menu item to Appearance menu and allow for user with capability 'edit_theme_options'
    */
    static function uwdgh_alerts_menu_item() {
			add_submenu_page('themes.php', 'Alert Banner', 'Alert Banner', 'edit_theme_options', 'uwdgh-alerts', array( __CLASS__, 'uwdgh_admin_features_options_page_alerts' ));
		}

		/**
		 * Callback function for hook admin_bar_menu
		 */
		static function uwdgh_alerts_admin_bar_item( WP_Admin_Bar $wp_admin_bar ) {

			if ( ! current_user_can( 'edit_theme_options' ) ) {
				return;
			}
	
			$wp_admin_bar->add_menu( array(
				'id'    => 'uwdgh-alerts',
				'parent' => null, //e.g. 'site-name'
				'group'  => null,
				'title' => __( 'Alert Banner', 'uwdgh-admin-features' ), //you can use img tag with image link. it will show the image icon Instead of the title.
				'href'  => admin_url('themes.php?page=uwdgh-alerts'),
				'meta' => [
					'title' => __( 'Alert Banner', 'uwdgh-admin-features' ), //This title will show on hover
				]
			) );

		}
		
    /**
    * Alerts tab
    */
    static function uwdgh_admin_features_tab_alerts() {
    	global $uwdgh_admin_features_active_tab; ?>
    	<a class="nav-tab <?php echo $uwdgh_admin_features_active_tab == 'alerts' || '' ? 'nav-tab-active' : ''; ?>" href="<?php echo admin_url( 'options-general.php?page=uwdgh-admin-features&tab=alerts' ); ?>"><?php _e( 'Alert Banner', 'uwdgh-admin-features' ); ?> </a>
    	<?php
    }
		
    /**
    * Alerts content
    */
    static function uwdgh_admin_features_options_page_alerts() {
      global $uwdgh_admin_features_active_tab;
      // if ( '' || 'alerts' != $uwdgh_admin_features_active_tab )
      //   return;

			$alerts_submenu_page = admin_url( "themes.php?page=uwdgh-alerts" );
			$alerts_submenu_page_updated = admin_url( "themes.php?page=uwdgh-alerts&settings-updated=true" );
      if ( ( '' || 'alerts' != $uwdgh_admin_features_active_tab ) && ( ( UWDGH_AdminFeatures::$requestpage != $alerts_submenu_page ) && ( UWDGH_AdminFeatures::$requestpage != $alerts_submenu_page_updated ) ) )
        return;
			
			$screen = get_current_screen();
			if ( 'appearance_page_uwdgh-alerts' == $screen->base ) {
				settings_errors();
			}
      ?>
      <h3><?php _e('Public Alert Banner','uwdgh-admin-features');?></h3>

      <form action="options.php" method="post" id="uwdgh-admin-features-options-form">
        <?php settings_fields(UWDGH_AdminFeatures_AFFIX.'_options_alerts'); ?>
        <table class="form-table">
          <tr class="even" valign="top">
            <th scope="row">
              <label for="uwdgh_admin_features_enable_alert">
                <?php _e('Enable the alert','uwdgh-admin-features');?>
              </label>
            </th>
            <td>
              <input type="checkbox" id="uwdgh_admin_features_enable_alert" name="uwdgh_admin_features_enable_alert"  value="1" <?php checked(1, get_option(UWDGH_AdminFeatures_AFFIX.'_enable_alert'), true); ?> />
              <span><em>(<?php _e('Default: unchecked','uwdgh-admin-features');?>)</em></span>
              <p><?php _e('When enabled, a persistent alert banner will display at the top of the page for all site visitors.','uwdgh-admin-features');?></p>
            </td>
          </tr>
					<tr class="odd" valign="top">
							<th scope="row">
								<label for="uwdgh_admin_features_alert_text">
									<?php _e('Alert text','uwdgh-admin-features');?>
								</label>
							</th>
							<td>
								<?php
								$_editor_id = 'uwdgh_admin_features_alert_text';
								$_editor_settings = array (
									'wpautop' => false,	//disables WP's auto paragraphing, keeping the P element in the source code of the editor
									'media_buttons' => false,	//disable Media
									'textarea_rows' => 7,
									'teeny' => false,
									'textarea_name' => $_editor_id,
									'tinymce' => array(
																'toolbar1' => 'formatselect | bold italic | link unlink | alignleft aligncenter alignright | undo redo | removeformat pastetext',
																'selector' => 'textarea',
																'block_formats' => 'Paragraph=p;Header 1=h1;Header 2=h2;Header 3=h3;Header 4=h4;Header 5=h5;Header 6=h6',	//list of formats to be displayed under formatselect dropdown
																'toolbar2' => '',
																'toolbar3' => '',
																'toolbar4' => ''
															),
									
									'quicktags' => false,	//disables the Visual and Text tabs
								);
								wp_editor( html_entity_decode(get_option(UWDGH_AdminFeatures_AFFIX.'_alert_text')), $_editor_id, $_editor_settings ); 
								?>
							</td>
					</tr>
					<tr class="even" valign="top">
							<th scope="row">
								<label for="uwdgh_admin_features_alert_type">
									<?php _e('Alert type','uwdgh-admin-features');?>
								</label>
							</th>
							<td>
								<select id="uwdgh_admin_features_alert_type" name="uwdgh_admin_features_alert_type">
									<option class="primary" value="primary" <?php selected( get_option(UWDGH_AdminFeatures_AFFIX.'_alert_type'), 'primary' ); ?>><?php _e('Primary','uwdgh-admin-features');?></option>
									<option class="secondary" value="secondary" <?php selected( get_option(UWDGH_AdminFeatures_AFFIX.'_alert_type'), 'secondary' ); ?>><?php _e('Secondary','uwdgh-admin-features');?></option>
									<option class="success" value="success" <?php selected( get_option(UWDGH_AdminFeatures_AFFIX.'_alert_type'), 'success' ); ?>><?php _e('Success','uwdgh-admin-features');?></option>
									<option class="danger" value="danger" <?php selected( get_option(UWDGH_AdminFeatures_AFFIX.'_alert_type'), 'danger' ); ?>><?php _e('Danger','uwdgh-admin-features');?></option>
									<option class="warning" value="warning" <?php selected( get_option(UWDGH_AdminFeatures_AFFIX.'_alert_type'), 'warning' ); ?>><?php _e('Warning','uwdgh-admin-features');?></option>
									<option class="info" value="info" <?php selected( get_option(UWDGH_AdminFeatures_AFFIX.'_alert_type'), 'info' ); ?>><?php _e('Info','uwdgh-admin-features');?></option>
									<option class="light" value="light" <?php selected( get_option(UWDGH_AdminFeatures_AFFIX.'_alert_type'), 'light' ); ?>><?php _e('Light','uwdgh-admin-features');?></option>
									<option class="dark" value="dark" <?php selected( get_option(UWDGH_AdminFeatures_AFFIX.'_alert_type'), 'dark' ); ?>><?php _e('Dark','uwdgh-admin-features');?></option>
									<option class="uw" value="uw" <?php selected( get_option(UWDGH_AdminFeatures_AFFIX.'_alert_type'), 'uw' ); ?>><?php _e('University of Washington','uwdgh-admin-features');?></option>
									<option class="uwspiritgold" value="uwspiritgold" <?php selected( get_option(UWDGH_AdminFeatures_AFFIX.'_alert_type'), 'uwspiritgold' ); ?>><?php _e('University of Washington Spirit Gold','uwdgh-admin-features');?></option>
								</select>
								<p class="description"><?php _e("For assistive technologies, the Alert Banner will create an ARIA live region on the page. Different alert types will set a different aria-live POLITENESS_SETTING for the live region.<br>
								Alert types 'danger' and 'warning' will set an assertive live region. These should only be used for time-sensitive/critical notifications that absolutely require the user's immediate attention. These two types will interrupt any announcement a screen reader is currently making.
								<br>
								All other types will set a polite live region. The screen reader will speak changes whenever the user is idle.",'uwdgh-admin-features');?>
								</p>
							</td>
					</tr>
        </table>
        <?php submit_button(); ?>
      </form>
			<table>
				<tr>
					<td style="vertical-align: top;">
						<h3><?php _e('Alert type reference','uwdgh-admin-features');?></h3>
						<div id="uwdgh_admin_features_alert_type_reference">
							<input type="text" class="regular-text primary" disabled="" value="Primary alert"></input><span class="description">POLITENESS_SETTING <code>polite</code></span><br>
							<input type="text" class="regular-text secondary" disabled="" value="Secondary alert"></input><span class="description">POLITENESS_SETTING <code>polite</code></span><br>
							<input type="text" class="regular-text success" disabled="" value="Success alert"></input><span class="description">POLITENESS_SETTING <code>polite</code></span><br>
							<input type="text" class="regular-text danger" disabled="" value="Danger alert"></input><span class="description">POLITENESS_SETTING <code>assertive</code></span><br>
							<input type="text" class="regular-text warning" disabled="" value="Warning alert"></input><span class="description">POLITENESS_SETTING <code>assertive</code></span><br>
							<input type="text" class="regular-text info" disabled="" value="Info alert"></input><span class="description">POLITENESS_SETTING <code>polite</code></span><br>
							<input type="text" class="regular-text light" disabled="" value="Light alert"></input><span class="description">POLITENESS_SETTING <code>polite</code></span><br>
							<input type="text" class="regular-text dark" disabled="" value="Dark alert"></input><span class="description">POLITENESS_SETTING <code>polite</code></span><br>
							<input type="text" class="regular-text uw" disabled="" value="University of Washington"></input><span class="description">POLITENESS_SETTING <code>polite</code></span><br>
							<input type="text" class="regular-text uwspiritgold" disabled="" value="University of Washington Spirit Gold"><span class="description">POLITENESS_SETTING <code>polite</code></span></input><br>
						</div>
					</td>
				</tr>
			</table>
			<?php
    }

		/**
		 * A callback function that sanitizes the alert text.
		 */
		static function uwdgh_alert_sanitize_textarea_field() {
			// Sanitize a multiline string from user input.
			//return sanitize_textarea_field( $_POST['uwdgh_admin_features_alert_text'] );

			// tinyMCE:
			// Remove slashes from a string or recursively removes slashes from strings within an array.
			// Convert all applicable characters to HTML entities.
			return wp_unslash( htmlentities( $_POST['uwdgh_admin_features_alert_text'] ) );
		}
		
		/**
    * Enqueue front-end js
    */
    static function uwdgh_alert_enqueue_script() {
			if ( get_option(UWDGH_AdminFeatures_AFFIX.'_enable_alert') == false )
				return;
				
			// register js
      wp_register_script('uwdgh_alerts', plugin_dir_url(__FILE__) . '../includes/js/uwdgh-alerts.js', array('jquery'), null, true);
      wp_enqueue_script('uwdgh_alerts');
			
			// add front-end css
      wp_enqueue_style( 'uwdgh_alerts', plugin_dir_url(__FILE__) . '../includes/css/uwdgh-alerts.css' );
     
			// localize script with 'UWDGH_Alerts' object and 'admin_ajax_url' key => value.
			// UWDGH_Alerts.admin_ajax_url to perform the jQuery Post.
			$UWDGH_Alerts = array( 'admin_ajax_url' => admin_url('admin-ajax.php') );
			wp_localize_script( 'uwdgh_alerts', 'UWDGH_Alerts', $UWDGH_Alerts );
    }

    /**
    * wp_ajax_ action callback
    */
		static function uwdgh_alert_get() {
			if ( get_option(UWDGH_AdminFeatures_AFFIX.'_enable_alert') == false )
				return;
			
			// create return array
			$_retval = array(
				'alert_type' => get_option(UWDGH_AdminFeatures_AFFIX.'_alert_type'),
				'alert_text' => do_shortcode( html_entity_decode( get_option(UWDGH_AdminFeatures_AFFIX.'_alert_text') ) ),
			);
			// echo the response
			echo json_encode($_retval, JSON_PRETTY_PRINT);
	    // Don't forget to stop execution afterward.
	    wp_die();
		}

    /**
    *
    * @param type $notice
    */
    public static function alert_banner_admin_notice() {
      ?>
      <div class="notice notice-info is-dismissible">
          <p><?php _e( "The public alert banner is enabled. Manage the alert banner in <a href='" . admin_url( 'themes.php?page=uwdgh-alerts' ) . "'>appearance</a>",'uwdgh-admin-features' ); ?>.</p>
      </div>
      <?php
    }

		/**
    * Add options on activation
    */
    static function uwdgh_admin_features_activate() {
      add_option(UWDGH_AdminFeatures_AFFIX.'_enable_alert', 0);
      add_option(UWDGH_AdminFeatures_AFFIX.'_alert_text', '');
      add_option(UWDGH_AdminFeatures_AFFIX.'_alert_type', '');
    }

		/**
    * Dispose plugin option upon plugin deactivation
    */
    static function uwdgh_admin_features_deactivate() {
      update_option(UWDGH_AdminFeatures_AFFIX.'_enable_alert', 0);
      //update_option(UWDGH_AdminFeatures_AFFIX.'_alert_text', '');
      //update_option(UWDGH_AdminFeatures_AFFIX.'_alert_type', '');
    }

    /**
    * Dispose plugin option upon plugin deletion
    */
    static function uwdgh_admin_features_uninstall() {
      // remove options
      delete_option(UWDGH_AdminFeatures_AFFIX.'_enable_alert');
      delete_option(UWDGH_AdminFeatures_AFFIX.'_alert_text');
      delete_option(UWDGH_AdminFeatures_AFFIX.'_alert_type');
    }

	}
	
  New UWDGH_Alerts;

}