<?php

if ( !class_exists( 'UWDGH_PostEnhancements' ) ) {
    
    class UWDGH_PostEnhancements {

        /**
         * Class constructor
         */
        function __construct() {

            // register setting
            register_setting(UWDGH_AdminFeatures_AFFIX.'_options_post_enhancements',UWDGH_AdminFeatures_AFFIX.'_enable_data_columns', array('default' => 1,));

            // Capture user login and add it as timestamp in user meta data
            add_action( 'wp_login', array( __CLASS__, 'uwdgh_user_last_login' ), 10, 2 );

            // data columns
            if ( get_option(UWDGH_AdminFeatures_AFFIX.'_enable_data_columns') ) {
                // Add the modified and ID columns to the posts and pages type
                add_filter( 'manage_pages_columns', array( __CLASS__, 'uwdgh_add_modified_column' ) );
                add_filter( 'manage_pages_columns', array( __CLASS__, 'uwdgh_add_id_column' ) );
                add_filter( 'manage_posts_columns', array( __CLASS__, 'uwdgh_add_modified_column' ) );
                add_filter( 'manage_posts_columns', array( __CLASS__, 'uwdgh_add_id_column' ) );
                // Add the ID column to the media library
                add_filter( 'manage_media_columns', array( __CLASS__, 'uwdgh_add_id_column' ) );
                // Add the custom columns to the users type
                add_filter( 'manage_users_columns', array( __CLASS__, 'uwdgh_add_users_column' ) );
                // Add the data to the custom column
                add_action( 'manage_pages_custom_column' , array( __CLASS__, 'uwdgh_add_modified_column_data' ), 10, 2 );
                add_action( 'manage_pages_custom_column' , array( __CLASS__, 'uwdgh_add_id_column_data' ), 10, 2 );
                add_action( 'manage_posts_custom_column' , array( __CLASS__, 'uwdgh_add_modified_column_data'), 10, 2 );
                add_action( 'manage_posts_custom_column' , array( __CLASS__, 'uwdgh_add_id_column_data'), 10, 2 );
                add_action( 'manage_media_custom_column' , array( __CLASS__, 'uwdgh_add_id_column_data'), 10, 2 );
                add_action( 'manage_users_custom_column' , array( __CLASS__, 'uwdgh_add_users_column_data'), 10, 3 );
                // Make the custom column sortable
                add_filter( 'manage_edit-page_sortable_columns', array( __CLASS__, 'uwdgh_add_custom_column_make_sortable' ) );
                add_filter( 'manage_edit-post_sortable_columns', array( __CLASS__, 'uwdgh_add_custom_column_make_sortable' ) );
                // Add custom column sort request to post list page
                add_action( 'load-edit.php', array( __CLASS__, 'uwdgh_add_custom_column_sort_request' ) );
                // Add column styling
                add_action( 'admin_head', array( __CLASS__, 'uwdgh_add_custom_column_css' ) );
            }
        }
        
        /**
        * Admin columns tab
        */
        static function uwdgh_admin_features_tab_post_enhancements() {
            global $uwdgh_admin_features_active_tab; ?>
            <a class="nav-tab <?php echo $uwdgh_admin_features_active_tab == 'post-enhancements' || '' ? 'nav-tab-active' : ''; ?>" href="<?php echo admin_url( 'options-general.php?page=uwdgh-admin-features&tab=post-enhancements' ); ?>"><?php _e( 'Post enhancements', 'uwdgh-admin-features' ); ?> </a>
            <?php
        }
        
        /**
        * Data columns page
        */
        static function uwdgh_admin_features_options_page_post_enhancements() {
            global $uwdgh_admin_features_active_tab;
            if ( '' || 'post-enhancements' != $uwdgh_admin_features_active_tab )
                return;
            ?>
            <h3><?php _e('Post enhancements','uwdgh-admin-features');?></h3>
            <form action="options.php" method="post" id="uwdgh-admin-features-options-form">
            <?php settings_fields(UWDGH_AdminFeatures_AFFIX.'_options_post_enhancements'); ?>
                <table class="form-table">
                    <tr class="even" valign="top">
                        <th scope="row">
                            <label for="uwdgh_admin_features_enable_data_columns">
                                <?php _e('Show data columns','uwdgh-admin-features');?>
                            </label>
                        </th>
                        <td>
                            <input type="checkbox" id="uwdgh_admin_features_enable_data_columns" name="uwdgh_admin_features_enable_data_columns"  value="1" <?php checked(1, get_option(UWDGH_AdminFeatures_AFFIX.'_enable_data_columns'), true); ?> />
                            <span><em>(<?php _e('Default: checked','uwdgh-admin-features');?>)</em></span>
                            <p class="description"><?php _e('When enabled, adds the following data columns:','uwdgh-admin-features');?>
                            </p>
                            <table class="widefat striped" style="max-width:400px;">
                                <thead style="font-style: italic;">
                                    <tr>
                                        <td><?php _e('Entity','uwdgh-admin-features');?></td>
                                        <td><?php _e('Column','uwdgh-admin-features');?></td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><?php _e('Posts','uwdgh-admin-features');?></td>
                                        <td><?php _e('Last Modified','uwdgh-admin-features');?></td>
                                    </tr>
                                    <tr>
                                        <td><?php _e('Posts','uwdgh-admin-features');?></td>
                                        <td><?php _e('ID','uwdgh-admin-features');?></td>
                                    </tr>
                                    <tr>
                                        <td><?php _e('Pages','uwdgh-admin-features');?></td>
                                        <td><?php _e('Last Modified','uwdgh-admin-features');?></td>
                                    </tr>
                                    <tr>
                                        <td><?php _e('Pages','uwdgh-admin-features');?></td>
                                        <td><?php _e('ID','uwdgh-admin-features');?></td>
                                    </tr>
                                    <tr>
                                        <td><?php _e('Media (list mode)','uwdgh-admin-features');?></td>
                                        <td><?php _e('ID','uwdgh-admin-features');?></td>
                                    </tr>
                                    <tr>
                                        <td><?php _e('Users','uwdgh-admin-features');?></td>
                                        <td><?php _e('User ID','uwdgh-admin-features');?></td>
                                    </tr>
                                    <tr>
                                        <td><?php _e('Users','uwdgh-admin-features');?></td>
                                        <td><?php _e('Last Login','uwdgh-admin-features');?></td>
                                    </tr>
                                    <tr>
                                        <td><?php _e('Users','uwdgh-admin-features');?></td>
                                        <td><?php _e('Registered Since','uwdgh-admin-features');?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </table>
            <?php submit_button(); ?>
            </form>
            <?php 
        }

        // Capture user login and add it as timestamp in user meta data
        static function uwdgh_user_last_login( $user_login, $user ) {
          update_user_meta( $user->ID, 'uwdgh-admin-columns_user_last_login', time() );
        }
    
        // Add the modified column to the post type
        static function uwdgh_add_modified_column( $columns ) {
          $columns['uwdgh_modified'] = 'Last Modified';
          return $columns;
        }
    
        // Add the id column to the post type
        static function uwdgh_add_id_column( $columns  ) {
          $columns['uwdgh_id'] = 'ID';
          return $columns;
        }
    
        // Add the custom column to the post type
        static function uwdgh_add_users_column( $columns ) {
          $columns['uwdgh_user_id'] = 'User ID';
          $columns['uwdgh_user_last_login'] = 'Last Login';
          $columns['uwdgh_user_registered_since'] = 'Registered Since';
          return $columns;
        }
    
        // Add the data to the custom column
        static function uwdgh_add_modified_column_data( $column, $post_id ) {
          switch ( $column ) {
            case 'uwdgh_modified' :
              $date_format = 'Y/m/d';
              $datetime_format = 'Y/m/d g:i:s a';
              $post = get_post( $post_id );
              echo '<abbr title="' . get_post_modified_time( $datetime_format, false, $post ) . '">' . get_the_modified_date( $date_format, $post ) . '</abbr><br>by: ' . get_the_modified_author(); // the data that is displayed in the column
              break;
          }
        }
        // Add the id column
        static function uwdgh_add_id_column_data( $column, $post_id ) {
          switch ( $column ) {
            case 'uwdgh_id' :
              echo '<span title="' . $post_id . '">' . $post_id . '</span>';
              break;
          }
        }
    
        // Add the data to the custom column
        static function uwdgh_add_users_column_data( $value, $column, $user_id ) {
          switch ($column) {
            case 'uwdgh_user_id':
              return $user_id;
              break;
            case 'uwdgh_user_last_login':
              $last_login = get_the_author_meta('uwdgh-admin-columns_user_last_login', $user_id);
              if ( !empty($last_login) ) {
                $the_login_date = human_time_diff($last_login);
                return $the_login_date;
              }
              return $value;
              break;
                    case 'uwdgh_user_registered_since':
                        $udata = get_userdata( $user_id );
                    $registered = $udata->user_registered;
                        return date( "M j, Y", strtotime( $registered ) );
                        break;
          }
        }
    
        // Make the custom column sortable
        static function uwdgh_add_custom_column_make_sortable( $columns ) {
          $columns['uwdgh_modified'] = 'uwdgh_modified';
          $columns['uwdgh_id'] = 'uwdgh_id';
          return $columns;
        }
    
        // Add custom column sort request to post list page
        static function uwdgh_add_custom_column_sort_request() {
          add_filter( 'request', array( __CLASS__, 'uwdgh_add_custom_column_do_sortable' ) );
        }
    
        // Handle the custom column sorting
        static function uwdgh_add_custom_column_do_sortable( $vars ) {
          // check if sorting has been applied
          if ( isset( $vars['orderby'] ) && 'uwdgh_modified' == $vars['orderby'] ) {
            // apply the sorting to the post list
            $vars = array_merge(
              $vars,
              array(
                'orderby' => 'post_modified'
              )
            );
          }
          if ( isset( $vars['orderby'] ) && 'uwdgh_id' == $vars['orderby'] ) {
            // apply the sorting to the post list
            $vars = array_merge(
              $vars,
              array(
                'orderby' => 'ID'
              )
            );
          }
          return $vars;
        }
    
        // Add column styling
        static function uwdgh_add_custom_column_css() {
          $output_css = '<style>
            .fixed .column-uwdgh_modified { width: 12.5%; }
            .fixed .column-uwdgh_id { width: 7.5%; }
            #uwdgh_user_id, .column-uwdgh_user_id { width: 7.5%; }
            @media screen and (min-width: 783px) { #uwdgh_user_id, .column-uwdgh_user_id { text-align: center; } }
            #uwdgh_user_last_login, #uwdgh_user_registered_since { width: 6.25%; }
            </style>';
          echo $output_css;
        }

        /**
        * Add options on activation
        */
        static function uwdgh_admin_features_activate() {
			// nothing to do here
            // add_option(UWDGH_AdminFeatures_AFFIX.'_print_post_status', 0);
        }

        /**
         * Dispose plugin option upon plugin deactivation
        */
        static function uwdgh_admin_features_deactivate() {
			// nothing to do here
            // update_option(UWDGH_AdminFeatures_AFFIX.'_enable_data_columns', 0);
            // update_option(UWDGH_AdminFeatures_AFFIX.'_print_post_status', 0);
        }

        /**
         * Dispose plugin option upon plugin deletion
        */
        static function uwdgh_admin_features_uninstall() {
            // remove options
            delete_option(UWDGH_AdminFeatures_AFFIX.'_enable_data_columns');
            delete_option(UWDGH_AdminFeatures_AFFIX.'_print_post_status');
        }

    }

    New UWDGH_PostEnhancements;

}