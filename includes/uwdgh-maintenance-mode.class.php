<?php

if ( !class_exists( 'UWDGH_MaintenanceMode' ) ) {

  class UWDGH_MaintenanceMode {

    function __construct() {

      // register settings
      register_setting(UWDGH_AdminFeatures_AFFIX.'_options_maintenance_mode',UWDGH_AdminFeatures_AFFIX.'_enable_maintenance_mode', array('default' => 0,));
      register_setting(UWDGH_AdminFeatures_AFFIX.'_options_maintenance_mode',UWDGH_AdminFeatures_AFFIX.'_maintenance_mode_text', array('default' => '',));

      // maintenance mode
      if ( get_option(UWDGH_AdminFeatures_AFFIX.'_enable_maintenance_mode') ) {
        // kill site for non-admin users
        add_action('get_header', array( __CLASS__, 'set_maintenance_mode') );
        // admin notice
        add_action( 'admin_notices',  array( __CLASS__, 'maintenance_mode_notice') );
      }

    }

    /**
    * Maintenance mode tab
    */
    static function uwdgh_admin_features_tab_maintenance_mode() {
    	global $uwdgh_admin_features_active_tab; ?>
    	<a class="nav-tab <?php echo $uwdgh_admin_features_active_tab == 'maintenance-mode' || '' ? 'nav-tab-active' : ''; ?>" href="<?php echo admin_url( 'options-general.php?page=uwdgh-admin-features&tab=maintenance-mode' ); ?>"><?php _e( 'Maintenance mode', 'uwdgh-admin-features' ); ?> </a>
    	<?php
    }

    /**
    * Maintenance mode content
    */
    static function uwdgh_admin_features_options_page_maintenance_mode() {
      global $uwdgh_admin_features_active_tab;
      if ( '' || 'maintenance-mode' != $uwdgh_admin_features_active_tab )
        return;
      // set default text
      if (UWDGH_AdminFeatures::$settingsupdated && trim(get_option(UWDGH_AdminFeatures_AFFIX.'_maintenance_mode_text'))=='') {
        update_option( UWDGH_AdminFeatures_AFFIX.'_maintenance_mode_text', self::maintenance_mode_text_default() );
      }
      ?>
      <h3><?php _e('Maintenance mode','uwdgh-admin-features');?></h3>

      <form action="options.php" method="post" id="uwdgh-admin-features-options-form">
        <?php settings_fields(UWDGH_AdminFeatures_AFFIX.'_options_maintenance_mode'); ?>
        <table class="form-table">
          <tr class="even" valign="top">
            <th scope="row">
              <label for="uwdgh_admin_features_enable_maintenance_mode">
                <?php _e('Put site into maintenance mode','uwdgh-admin-features');?>
              </label>
            </th>
            <td>
              <input type="checkbox" id="uwdgh_admin_features_enable_maintenance_mode" name="uwdgh_admin_features_enable_maintenance_mode"  value="1" <?php checked(1, get_option(UWDGH_AdminFeatures_AFFIX.'_enable_maintenance_mode'), true); ?> />
              <span><em>(<?php _e('Default: unchecked','uwdgh-admin-features');?>)</em></span>
              <p class="description"><?php _e('When enabled, only users with the "administrator" role or users with the "manage_options" capability are able to access your site to perform maintenance; all other visitors see the maintenance mode message configured below.','uwdgh-admin-features');?></p>
              <br>
              <textarea id="uwdgh_admin_features_maintenance_mode_text" name="uwdgh_admin_features_maintenance_mode_text" rows="5" cols="128" placeholder="<?php echo self::maintenance_mode_text_default(); ?>"><?php echo esc_textarea(get_option(UWDGH_AdminFeatures_AFFIX.'_maintenance_mode_text')); ?></textarea>
            </td>
          </tr>
        </table>
        <?php submit_button(); ?>
      </form>
      <?php
    }

    /**
    * Set Maintenance mode
    */
    static function set_maintenance_mode () {
      $user = wp_get_current_user();
      if ( ( (!in_array( 'administrator', (array) $user->roles ) || !current_user_can('manage_options')) ) || !is_user_logged_in() ) {
        wp_die( get_option(UWDGH_AdminFeatures_AFFIX.'_maintenance_mode_text'), esc_html('Maintenance mode ‹ ' . get_bloginfo( 'name' )) );
      }
    }

    /**
    * Returns standard placeholder text for maintenance mode
    */
    static function maintenance_mode_text_default() {
      $defaulttext = __('Briefly unavailable for scheduled maintenance. Check back in a minute.','uwdgh-admin-features') . PHP_EOL ;
      return $defaulttext;
    }

    /**
    *
    * @param type $notice
    */
    public static function maintenance_mode_notice() {
      ?>
      <div class="notice notice-info is-dismissible">
          <p><?php _e( "The website is in maintenance mode. Manage the maintenance mode in <a href='" . admin_url( 'options-general.php?page=uwdgh-admin-features&tab=maintenance-mode' ) . "'>settings</a>",'uwdgh-admin-features' ); ?>.</p>
      </div>
      <?php
    }

    /**
    * Dispose plugin option upon plugin deactivation
    */
    static function uwdgh_admin_features_deactivate() {
      update_option(UWDGH_AdminFeatures_AFFIX.'_enable_maintenance_mode', 0);
      //update_option(UWDGH_AdminFeatures_AFFIX.'_maintenance_mode_text', '');
    }

    /**
    * Dispose plugin option upon plugin deletion
    */
    static function uwdgh_admin_features_uninstall() {
      // remove options
      delete_option(UWDGH_AdminFeatures_AFFIX.'_enable_maintenance_mode');
      delete_option(UWDGH_AdminFeatures_AFFIX.'_maintenance_mode_text');
    }

  }

  New UWDGH_MaintenanceMode;

}
