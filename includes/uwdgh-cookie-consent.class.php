<?php

if ( !class_exists( 'UWDGH_CookieConsent' ) ) {

  class UWDGH_CookieConsent {

    function __construct() {

			/**
			 * implement hook admin_init
			 */
			add_action('admin_init', array( __CLASS__, 'uwdgh_cookie_consent_register_settings' ) );

      /**
       * implement hook wp_footer
       * implement filter hook wp_nav_menu_items
       */
      if ( get_option(UWDGH_AdminFeatures_AFFIX.'_enable_cookie_consent') ) {
        add_action( 'wp_footer', array( __CLASS__, 'uwdgh_cookie_consent_add_cookie_consent_script' ) );
        add_filter( 'wp_nav_menu_items', array( __CLASS__, 'add_cookie_consent_preferences_center_link' ), 11, 2  );
      }
      
    }
    
		/**
		 * Callback for hook admin_init
		 * Register plugin settings
		 */
		static function uwdgh_cookie_consent_register_settings() {
			
			// Option to enable cookie consent section
			register_setting(
				UWDGH_AdminFeatures_AFFIX.'_options_cookie_consent',		//settings group name
				UWDGH_AdminFeatures_AFFIX.'_enable_cookie_consent',		//name of an option to sanitize and save
				array('default' => 0,)		//Data used to describe the setting when registered
			);
			// Option for cookie consent banner type
			register_setting(
				UWDGH_AdminFeatures_AFFIX.'_options_cookie_consent',		//settings group name
				UWDGH_AdminFeatures_AFFIX.'_cookie_consent_notice_banner_type',		//name of an option to sanitize and save
				array('default' => '',)		//Data used to describe the setting when registered
			);

		}


    /**
    * Callback function to add Admin columns tab
    */
    static function uwdgh_admin_features_tab_cookie_consent() {
    	global $uwdgh_admin_features_active_tab; ?>
    	<a class="nav-tab <?php echo $uwdgh_admin_features_active_tab == 'cookie-consent' || '' ? 'nav-tab-active' : ''; ?>" href="<?php echo admin_url( 'options-general.php?page=uwdgh-admin-features&tab=cookie-consent' ); ?>"><?php _e( 'Cookie consent', 'uwdgh-admin-features' ); ?> </a>
    	<?php
    }

    /**
    * Profiles settings page
    */
    static function uwdgh_admin_features_options_page_cookie_consent() {
      global $uwdgh_admin_features_active_tab;
      if ( '' || 'cookie-consent' != $uwdgh_admin_features_active_tab )
        return;
      ?>
      <h3><?php _e('Cookie consent','uwdgh-admin-features');?></h3>
      <p>This cookie consent integration uses Free Cookie Consent by <a href="https://www.freeprivacypolicy.com/free-cookie-consent/" target="_blank" rel="noreferrer">FreePrivacyPolicy.com</a>, and complies with <a href="http://data.europa.eu/eli/dir/2002/58/oj" target="_blank" rel="noreferrer">ePrivacy Directive</a> and <a href="https://gdpr-info.eu/" target="_blank" rel="noreferrer">GDPR</a>.</p>
      <form action="options.php" method="post" id="uwdgh-admin-features-options-form">
        <?php settings_fields(UWDGH_AdminFeatures_AFFIX.'_options_cookie_consent'); ?>
        <table class="form-table">
          <tr class="odd" valign="top">
            <th scope="row">
              <label for="uwdgh_admin_features_enable_cookie_consent">
                <?php _e('Enable Cookie consent','uwdgh-admin-features');?>
              </label>
            </th>
            <td>
              <input type="checkbox" id="uwdgh_admin_features_enable_cookie_consent" name="uwdgh_admin_features_enable_cookie_consent"  value="1" <?php checked(1, get_option(UWDGH_AdminFeatures_AFFIX.'_enable_cookie_consent'), true); ?> />
              <span><em>(<?php _e('Default: unchecked','uwdgh-admin-features');?>)</em></span>
              <p class="description"><?php _e('When checked, the FreePrivacyPolicy Cookie consent manager solution will be enabled on the website.','uwdgh-admin-features');?>
              </p>
            </td>
          </tr>
					<tr class="even" valign="top">
							<th scope="row">
								<label for="uwdgh_admin_features_cookie_consent_notice_banner_type">
									<?php _e('Notice banner type','uwdgh-admin-features');?>
								</label>
							</th>
							<td>
								<select id="uwdgh_admin_features_cookie_consent_notice_banner_type" name="uwdgh_admin_features_cookie_consent_notice_banner_type">
									<option class="" value="simple" <?php selected( get_option(UWDGH_AdminFeatures_AFFIX.'_cookie_consent_notice_banner_type'), 'simple' ); ?>><?php _e('Simple','uwdgh-admin-features');?></option>
									<option class="" value="headline" <?php selected( get_option(UWDGH_AdminFeatures_AFFIX.'_cookie_consent_notice_banner_type'), 'headline' ); ?>><?php _e('Headline','uwdgh-admin-features');?></option>
									<option class="" value="interstitial" <?php selected( get_option(UWDGH_AdminFeatures_AFFIX.'_cookie_consent_notice_banner_type'), 'interstitial' ); ?>><?php _e('Interstitial','uwdgh-admin-features');?></option>
									<option class="" value="standalone" <?php selected( get_option(UWDGH_AdminFeatures_AFFIX.'_cookie_consent_notice_banner_type'), 'standalone' ); ?>><?php _e('Standalone','uwdgh-admin-features');?></option>
								</select>
								<p class="description"><?php _e("Default is 'Simple'",'uwdgh-admin-features');?>
								</p>
							</td>
					</tr>
        </table>
        <?php submit_button(); ?>
      </form>
      <h4>How to integrate Cookie Consent on your website</h4>
      <ol>
        <li>Enable the Cookie consent option</li>
        <li>For each tagged <span class="alert alert-info">&lt;script&gt;</span>, add the corresponding consent level:
        </li>
            <ul>
                <li><span class="alert alert-info">data-cookie-consent="strictly-necessary"</span>
                    for strictly necessary scripts that are loaded regardless of user choice
                </li>
                <li><span class="alert alert-info">data-cookie-consent="functionality"</span> for
                    functionality scripts, such as language preference scripts
                </li>
                <li><span class="alert alert-info">data-cookie-consent="tracking"</span> for
                    tracking scripts, such as Google Analytics
                </li>
                <li><span class="alert alert-info">data-cookie-consent="targeting"</span> for
                    targeting scripts, such as Google Ads remarketing
                </li>
            </ul>
        <li>Add the link that users can use to open Preferences Center to your site's theme.
          <ul>
            <li>
              <pre>     
              &lt;!-- Below is the link that users can use to open Preferences Center to change their preferences. Do not modify the ID parameter. Place it where appropriate, style it as needed. --&gt;
              &lt;a href="#" id="open_preferences_center"&gtCookie preferences&lt;/a&gt;
              </pre>
            </li>
            <li><strong>Note!</strong> The link will be added automatically to the footer-links menu in sites using the "UW WordPress Theme" template.</li>
          </ul>
        </li>
        <li>Done.</li>
      </ol>
      <hr>
    <?php 
		}

    /**
     * callback function for hook wp_footer
     */
    static function uwdgh_cookie_consent_add_cookie_consent_script() {

			?>
        <!-- Cookie Consent by FreePrivacyPolicy.com https://www.FreePrivacyPolicy.com -->
        <script type="text/javascript" src="//www.freeprivacypolicy.com/public/cookie-consent/4.2.0/cookie-consent.js"></script>
        <script type="text/javascript">
        document.addEventListener('DOMContentLoaded', function () {
          cookieconsent.run({
            "notice_banner_type":"<?php echo get_option(UWDGH_AdminFeatures_AFFIX.'_cookie_consent_notice_banner_type', 'simple' ); ?>",
            "consent_type":"express",
            "palette":"light",
            "language":"en",
            "page_load_consent_levels":["strictly-necessary"],
            "notice_banner_reject_button_hide":false,
            "preferences_center_close_button_hide":false,
            "page_refresh_confirmation_buttons":false,
            "website_name":"UW Department of Global Health",
            "website_privacy_policy_url":"https://www.washington.edu/online/privacy/",
          });
        });
        </script>
			<?php

    }
    
    /**
     * callback function for filter hook wp_nav_menu_items
     */
    static function add_cookie_consent_preferences_center_link( $items, $args ) {
      
      $this_theme = wp_get_theme();

      // do_action( 'qm/debug', $this_theme->template );
      // do_action( 'qm/debug', $items );
      // do_action( 'qm/debug', $args->menu );
      // do_action( 'qm/debug', $args->theme_location );

      // add list item link to footer-links in sites using template: uw_wp_theme
      if ( ( $this_theme->template == 'uw_wp_theme' ) && in_array( $args->theme_location, array('footer-links') ) ) {
        $cookie_consent_preferences = <<<PREFERENCES_CENTER_LINK
        <li id="menu-item-uwdgh-cookie-consent" class="menu-item menu-item-type-custom menu-item-object-custom">
        <a href="#" id="open_preferences_center">Cookie preferences</a></li>
        PREFERENCES_CENTER_LINK;
        $items .= $cookie_consent_preferences;
      }

      return $items;

    }

    /**
    * Update plugin option upon plugin deactivation
    */
    static function uwdgh_cookie_consent_deactivate() {
        // update_option(UWDGH_AdminFeatures_AFFIX.'_enable_cookie_consent', 0);
    }
  
    /**
     * Dispose plugin option upon plugin deletion
    */
    static function uwdgh_cookie_consent_uninstall() {
        delete_option(UWDGH_AdminFeatures_AFFIX.'_enable_cookie_consent');
    }

    
  }

  New UWDGH_CookieConsent;

}
