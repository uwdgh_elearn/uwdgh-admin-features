<?php
/*
Plugin Name:  UW DGH | Admin Features
Plugin URI:   https://bitbucket.org/uwdgh_elearn/uwdgh-admin-features
Update URI:   https://bitbucket.org/uwdgh_elearn/uwdgh-admin-features
Description:  Adds administrator features to the website.
Author:       Department of Global Health - University of Washington
Author URI:   https://depts.washington.edu/dghweb/
Version:      1.12
License:      GPLv2 or later
License URI:  https://www.gnu.org/licenses/gpl-2.0.html
Text Domain:  uwdgh-admin-features
Domain Path:
Bitbucket Plugin URI:  https://bitbucket.org/uwdgh_elearn/uwdgh-admin-features

"UW DGH | Admin Features" is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.

"UW DGH | Admin Features" is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with "UW DGH | Admin Features". If not, see https://www.gnu.org/licenses/gpl-2.0.html.
 */
?>
<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Basic plugin definitions
 *
 * @package UW DGH | Admin Features
 * @since 1.9.3
 */
if( ! defined( 'UWDGH_AdminFeatures_NAME' ) ) {
	define( 'UWDGH_AdminFeatures_NAME', 'UW DGH | Admin Features' ); // Name of plugin
}
if( ! defined( 'UWDGH_AdminFeatures_AFFIX' ) ) {
	define( 'UWDGH_AdminFeatures_AFFIX', 'uwdgh_admin_features' ); // Plugin affix
}
if( ! defined( 'UWDGH_AdminFeatures_DIR' ) ) {
	define( 'UWDGH_AdminFeatures_DIR', dirname( __FILE__ ) ); // Plugin dir
}
if( ! defined( 'UWDGH_AdminFeatures_PLUGIN' ) ) {
	define( 'UWDGH_AdminFeatures_PLUGIN', plugin_basename( __FILE__ ) ); // Plugin
}

if ( !class_exists( 'UWDGH_AdminFeatures' ) ) {

  // includes
  require_once( plugin_dir_path(__FILE__) . 'includes/uwdgh-maintenance-mode.class.php' );
  require_once( plugin_dir_path(__FILE__) . 'includes/uwdgh-plugin-update-notification.class.php' );
  require_once( plugin_dir_path(__FILE__) . 'includes/uwdgh-theme-update-notification.class.php' );
  require_once( plugin_dir_path(__FILE__) . 'includes/uwdgh-alerts.class.php' );
  require_once( plugin_dir_path(__FILE__) . 'includes/uwdgh-profile-settings.class.php' );
  require_once( plugin_dir_path(__FILE__) . 'includes/uwdgh-post-enhancements.class.php' );
  require_once( plugin_dir_path(__FILE__) . 'includes/uwdgh-cookie-consent.class.php' );

  class UWDGH_AdminFeatures {

    static $settingsupdated;
		static $requestpage;

    // class initializaton
    function __construct() {
      // IsPostback
      self::$settingsupdated = false;

			// Request page
			self::$requestpage = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://" 
			. $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

      // add function catching settings-updated
      add_action( 'admin_menu' , array( __CLASS__, UWDGH_AdminFeatures_AFFIX.'_settings_updated' ) );

      // Add plugin settings link to Plugins page
      add_filter( 'plugin_action_links_'.UWDGH_AdminFeatures_PLUGIN, array( __CLASS__, UWDGH_AdminFeatures_AFFIX.'_add_settings_link' ) );
      // add menu item
      add_action( 'admin_menu' , array( __CLASS__, UWDGH_AdminFeatures_AFFIX.'_add_menu_item' ));
      // add admin css/js
			add_action('admin_enqueue_scripts', array( __CLASS__, UWDGH_AdminFeatures_AFFIX.'_enqueue_admin_css_js'));

			// register activation hooks
			if( function_exists('register_activation_hook') ) {
				register_activation_hook(__FILE__,array( 'UWDGH_Alerts', UWDGH_AdminFeatures_AFFIX.'_activate' ));
			}

      // register deactivation hooks for each feature
      if( function_exists('register_deactivation_hook') ) {
        register_deactivation_hook(__FILE__,array( __CLASS__, UWDGH_AdminFeatures_AFFIX.'_deactivate' ));
        register_deactivation_hook(__FILE__,array( 'UWDGH_MaintenanceMode', UWDGH_AdminFeatures_AFFIX.'_deactivate' ));
        register_deactivation_hook(__FILE__,array( 'UWDGH_PluginUpdateNotification', UWDGH_AdminFeatures_AFFIX.'_deactivate' ));
        register_deactivation_hook(__FILE__,array( 'UWDGH_ThemeUpdateNotification', UWDGH_AdminFeatures_AFFIX.'_deactivate' ));
        register_deactivation_hook(__FILE__,array( 'UWDGH_Alerts', UWDGH_AdminFeatures_AFFIX.'_deactivate' ));
        register_deactivation_hook(__FILE__,array( 'UWDGH_ProfileSettings', UWDGH_AdminFeatures_AFFIX.'_deactivate' ));
        register_deactivation_hook(__FILE__,array( 'UWDGH_PostEnhancements', UWDGH_AdminFeatures_AFFIX.'_deactivate' ));
        register_deactivation_hook(__FILE__,array( 'UWDGH_CookieConsent', UWDGH_AdminFeatures_AFFIX.'_deactivate' ));
      }
      // register uninstall hooks for each feature
      if( function_exists('register_uninstall_hook') ) {
        register_uninstall_hook(__FILE__,array( __CLASS__, UWDGH_AdminFeatures_AFFIX.'_uninstall' ));
        register_uninstall_hook(__FILE__,array( 'UWDGH_MaintenanceMode', UWDGH_AdminFeatures_AFFIX.'_uninstall' ));
        register_uninstall_hook(__FILE__,array( 'UWDGH_PluginUpdateNotification', UWDGH_AdminFeatures_AFFIX.'_uninstall' ));
        register_uninstall_hook(__FILE__,array( 'UWDGH_ThemeUpdateNotification', UWDGH_AdminFeatures_AFFIX.'_uninstall' ));
        register_uninstall_hook(__FILE__,array( 'UWDGH_Alerts', UWDGH_AdminFeatures_AFFIX.'_uninstall' ));
        register_uninstall_hook(__FILE__,array( 'UWDGH_ProfileSettings', UWDGH_AdminFeatures_AFFIX.'_uninstall' ));
        register_uninstall_hook(__FILE__,array( 'UWDGH_PostEnhancements', UWDGH_AdminFeatures_AFFIX.'_uninstall' ));
        register_uninstall_hook(__FILE__,array( 'UWDGH_CookieConsent', UWDGH_AdminFeatures_AFFIX.'_uninstall' ));
      }
    }

    /**
    * Admin css and js
    */
		static function uwdgh_admin_features_enqueue_admin_css_js() {
			wp_enqueue_style( 'uwdgh-admin-features-admin', plugin_dir_url(__FILE__) . 'admin/css/uwdgh-admin-features-admin.css' );
		}

    /**
    * Is Postback from submitting the option form
    */
    static function uwdgh_admin_features_settings_updated() {
      if(isset($_GET['settings-updated'])){
        self::$settingsupdated = $_GET['settings-updated'];
      }
    }

    /**
    * Add settings link
    */
    static function uwdgh_admin_features_add_settings_link( $links ) {
      $settings_link = '<a href="options-general.php?page=uwdgh-admin-features">' . __( 'Settings' ) . '</a>';
      array_push( $links, $settings_link );
      return $links;
    }

    /**
    * Admin menu item
    */
    static function uwdgh_admin_features_add_menu_item() {
      add_options_page(UWDGH_AdminFeatures_NAME, UWDGH_AdminFeatures_NAME, 'manage_options', 'uwdgh-admin-features', array( __CLASS__, UWDGH_AdminFeatures_AFFIX.'_options_settings_page' ));
      // add settings page tabs
      // welcome
      add_action( UWDGH_AdminFeatures_AFFIX.'_settings_tab', array( __CLASS__, UWDGH_AdminFeatures_AFFIX.'_tab_about' ), 1 );
      add_action( UWDGH_AdminFeatures_AFFIX.'_settings_content', array( __CLASS__, UWDGH_AdminFeatures_AFFIX.'_options_page_about' ) );
      // alerts
      add_action( UWDGH_AdminFeatures_AFFIX.'_settings_tab', array( 'UWDGH_Alerts', UWDGH_AdminFeatures_AFFIX.'_tab_alerts' ), 2 );
      add_action( UWDGH_AdminFeatures_AFFIX.'_settings_content', array( 'UWDGH_Alerts', UWDGH_AdminFeatures_AFFIX.'_options_page_alerts' ) );
      // maintenance mode
      add_action( UWDGH_AdminFeatures_AFFIX.'_settings_tab', array( 'UWDGH_MaintenanceMode', UWDGH_AdminFeatures_AFFIX.'_tab_maintenance_mode' ), 3 );
      add_action( UWDGH_AdminFeatures_AFFIX.'_settings_content', array( 'UWDGH_MaintenanceMode', UWDGH_AdminFeatures_AFFIX.'_options_page_maintenance_mode' ) );
      // profile settings
      add_action( UWDGH_AdminFeatures_AFFIX.'_settings_tab', array( 'UWDGH_ProfileSettings', UWDGH_AdminFeatures_AFFIX.'_tab_profile_settings' ), 4 );
      add_action( UWDGH_AdminFeatures_AFFIX.'_settings_content', array( 'UWDGH_ProfileSettings', UWDGH_AdminFeatures_AFFIX.'_options_page_profile_settings' ) );
      // plugin update notification
      add_action( UWDGH_AdminFeatures_AFFIX.'_settings_tab', array( 'UWDGH_PluginUpdateNotification', UWDGH_AdminFeatures_AFFIX.'_tab_plugin_update_notifications' ), 5 );
      add_action( UWDGH_AdminFeatures_AFFIX.'_settings_content', array( 'UWDGH_PluginUpdateNotification', UWDGH_AdminFeatures_AFFIX.'_options_page_plugin_update_notifications' ) );
      // theme update notification
      add_action( UWDGH_AdminFeatures_AFFIX.'_settings_tab', array( 'UWDGH_ThemeUpdateNotification', UWDGH_AdminFeatures_AFFIX.'_tab_theme_update_notifications' ), 6 );
      add_action( UWDGH_AdminFeatures_AFFIX.'_settings_content', array( 'UWDGH_ThemeUpdateNotification', UWDGH_AdminFeatures_AFFIX.'_options_page_theme_update_notifications' ) );
      // post enhancements
      add_action( UWDGH_AdminFeatures_AFFIX.'_settings_tab', array( 'UWDGH_PostEnhancements', UWDGH_AdminFeatures_AFFIX.'_tab_post_enhancements' ), 7 );
      add_action( UWDGH_AdminFeatures_AFFIX.'_settings_content', array( 'UWDGH_PostEnhancements', UWDGH_AdminFeatures_AFFIX.'_options_page_post_enhancements' ) );
      // cookie consent
      add_action( UWDGH_AdminFeatures_AFFIX.'_settings_tab', array( 'UWDGH_CookieConsent', UWDGH_AdminFeatures_AFFIX.'_tab_cookie_consent' ), 8 );
      add_action( UWDGH_AdminFeatures_AFFIX.'_settings_content', array( 'UWDGH_CookieConsent', UWDGH_AdminFeatures_AFFIX.'_options_page_cookie_consent' ) );
    }

    /**
    * Settings page
    */
    static function uwdgh_admin_features_options_settings_page() {
      global $uwdgh_admin_features_active_tab;
      $uwdgh_admin_features_active_tab = isset( $_GET['tab'] ) ? $_GET['tab'] : 'about';
      ?>
      <div class="wrap">
        <h2><?php _e(UWDGH_AdminFeatures_NAME,'uwdgh-admin-features');?></h2>
        <h2 class="nav-tab-wrapper">
        <?php do_action( UWDGH_AdminFeatures_AFFIX.'_settings_tab' ); ?>
        </h2>
        <?php
        do_action( UWDGH_AdminFeatures_AFFIX.'_settings_content' );
        ?>
      </div>
      <?php
    }

    /**
    * Welcome tab
    */
    static function uwdgh_admin_features_tab_about() {
    	global $uwdgh_admin_features_active_tab; ?>
    	<a class="nav-tab <?php echo $uwdgh_admin_features_active_tab == 'about' || '' ? 'nav-tab-active' : ''; ?>" href="<?php echo admin_url( 'options-general.php?page=uwdgh-admin-features&tab=about' ); ?>"><?php _e( 'About', 'uwdgh-admin-features' ); ?> </a>
    	<?php
    }

    /**
    * Welcome content
    */
    static function uwdgh_admin_features_options_page_about() {
      global $uwdgh_admin_features_active_tab;
      if ( '' || 'about' != $uwdgh_admin_features_active_tab )
        return;
      ?>
      <p><?php _e('This plugin has the following features.','uwdgh-admin-features');?></p>
			<div class="uwdgh-admin-features columns2">
				<a href="<?php echo admin_url( 'options-general.php?page=uwdgh-admin-features&tab=alerts' ); ?>" class="uwdgh-admin-features-card">
					<div class="card">
						<h3><?php _e('Alert banner','uwdgh-admin-features');?></h3>
						<p><?php _e('Set a persistent public alert banner.','uwdgh-admin-features');?></p>
					</div>
				</a>
				<a href="<?php echo admin_url( 'options-general.php?page=uwdgh-admin-features&tab=maintenance-mode' ); ?>" class="uwdgh-admin-features-card">
					<div class="card">
						<h3><?php _e('Maintenance mode','uwdgh-admin-features');?></h3>
						<p><?php _e('Set the site in maintenance mode.','uwdgh-admin-features');?></p>
					</div>
				</a>
				<a href="<?php echo admin_url( 'options-general.php?page=uwdgh-admin-features&tab=profile-settings' ); ?>" class="uwdgh-admin-features-card">
					<div class="card">
						<h3><?php _e('Profile settings','uwdgh-admin-features');?></h3>
						<p><?php _e("Manage settings for users profile page.",'uwdgh-admin-features');?></p>
					</div>
				</a>
				<a href="<?php echo admin_url( 'options-general.php?page=uwdgh-admin-features&tab=cookie-consent' ); ?>" class="uwdgh-admin-features-card">
					<div class="card">
						<h3><?php _e('Cookie consent','uwdgh-admin-features');?></h3>
						<p><?php _e("Add a Cookie Consent banner for your website for compliance with ePrivacy&nbsp;Directive and GDPR.",'uwdgh-admin-features');?></p>
            <?php if ( get_option(UWDGH_AdminFeatures_AFFIX.'_enable_cookie_consent') ) : ?>
              <p><?php _e('✔ Cookie consent is enabled.','uwdgh-admin-features'); ?></p>
            <?php endif; ?>
					</div>
				</a>
			</div>
			<div class="uwdgh-admin-features columns2">
				<a href="<?php echo admin_url( 'options-general.php?page=uwdgh-admin-features&tab=plugin-update-notification' ); ?>" class="uwdgh-admin-features-card">
					<div class="card">
						<h3><?php _e('Plugin update notifications','uwdgh-admin-features');?></h3>
						<p><?php _e('Disable individual plugin update notifications.','uwdgh-admin-features');?></p>
					</div>
				</a>
				<a href="<?php echo admin_url( 'options-general.php?page=uwdgh-admin-features&tab=theme-update-notification' ); ?>" class="uwdgh-admin-features-card">
					<div class="card">
						<h3><?php _e('Theme update notifications','uwdgh-admin-features');?></h3>
						<p><?php _e('Disable individual theme update notifications.','uwdgh-admin-features');?></p>
					</div>
				</a>
				<a href="<?php echo admin_url( 'options-general.php?page=uwdgh-admin-features&tab=post-enhancements'); ?>" class="uwdgh-admin-features-card">
					<div class="card">
						<h3><?php _e('Post enhancements','uwdgh-admin-features');?></h3>
						<p><?php _e('Visual and functional enhancement options.','uwdgh-admin-features');?></p>
					</div>
				</a>
		 	</div>
      <?php
    }

    /**
    * Dispose plugin option upon plugin deactivation
    */
    static function uwdgh_admin_features_deactivate() {
      // nothing to do here
    }

    /**
    * Dispose plugin option upon plugin deletion
    */
    static function uwdgh_admin_features_uninstall() {
      // nothing to do here
    }

  }

  New UWDGH_AdminFeatures;

}
