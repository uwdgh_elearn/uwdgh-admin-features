=== UW DGH | Admin Features ===
Contributors: dghweb, jbleys
Requires at least: 5.6
Tested up to: 6.7
Requires PHP: 7.2.23
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Adds administrator features to the website.

== Description ==
Adds administrator features to the website.

### Features

**Cookie consent**

**Public alert message**

**Maintenance mode option**

**Profile page settings**

**Disable plugin update notifications**

**Disable theme update notifications**

**Post enhancements**


== Changelog ==
**[unreleased]**

#### 1.12 / 2025-02-15
* Added Cookie consent feature, using [Free Cookie Consent](https://www.freeprivacypolicy.com/free-cookie-consent/) solution from FreePrivacyPolicy.com

#### 1.11.6 / 2025-02-11
* Allow the filtering of shortcodes in the alert text.

#### 1.11.5 / 2025-02-08
* Bug fix: correct unslashing alert text

#### 1.11.4 / 2025-02-08
* Added heading 1 to alert editor
* JS refactoring

#### 1.11.3 / 2025-02-06
* Renamed Tab Data Columns to Post Enhancements
* Added Alert Banner type UW Spirit Gold
* Tested for WordPress 6.7

#### 1.11.2 / 2024-02-03
* Renamed Alert Message to Alert banner
* Added Alert Banner item to admin bar

#### 1.11.1 / 2024-02-02
* Style update: reduce alert padding

#### 1.11 / 2023-12-27
* Added feature to manage settings on a users Profile page
* Added option to disable Application Passwords Section

#### 1.10.16 / 2023-12-22
* Minor code enhancements

#### 1.10.15 / 2023-12-05
* Tested with WordPress version 6.4
* Feature enhancement: ID column is now sortable
* Feature enhancement: uwdgh-alert div has added data-nosnippet attribute

#### 1.10.14 / 2023-07-10
* Bug fixes.

#### 1.10.13 / 2023-06-26
* Tested for PHP 8.x

#### 1.10.12 / 2023-06-08
* Bug fixes.

#### 1.10.11 / 2023-06-06
* Add Registered Since column to users table.
* Disable Alert Message background image on smaller screens.
* Tested for 6.2.
* Bug fixes.

#### 1.10.10 / 2023-03-08
* Optimize retention of option settings on deactivation.
* Bug fixes.

#### 1.10.9 / 2023-02-01
* Add localization functions around plugin texts.

#### 1.10.8 / 2023-01-25
* Enhanced the Alert Message for assistive technologies/screen readers.

#### 1.10.7 / 2023-01-25
* Update tinyMCE editor configuration for Alert Message.
* Add UW branded alert type.

#### 1.10.6 / 2023-01-25
* Update tinyMCE editor configuration for Alert Message.

#### 1.10.5 / 2023-01-24
* Update Alert Message textarea field to tinyMCE editor.

#### 1.10.4 / 2023-01-18
* Add Alert Message feature to Appearance menu and allow for users with edit_theme_options capability.

#### 1.10.3 / 2023-01-13
* Update admin info notices for Maintenance Mode and Alert Message features.

#### 1.10.2 / 2023-01-11
* Bug fix: admin ajax 400 bad request fixed with wp_ajax_nopriv_{$action} callback function for non-authenticated Ajax actions for logged-out users.
* Add Close button to Alert.

#### 1.10.1 / 2023-01-06
* Bug fixes

#### 1.10.0 / 2023-01-06
* Added Alert Message feature

#### 1.9.4 / 2022-08-31
* Fixed plugin header inconsistencies

#### 1.9.3 / 2022-03-03
* Bug fixes
* Due to refactoring of hooks and functions the plugin may need to be reactivated.

#### 1.9.2 / 2022-02-23
* Bug fixes

#### 1.9.1 / 2022-02-16
* Bug fix

#### 1.9 / 2022-02-16
* Added feature to disable plugin update notifications
* Added feature to disable theme update notifications
* Bug fixes

#### 1.8.3 / 2022-02-04
* Added ID column to Media Library (list mode)

#### 1.8.2 / 2021-10-21
* Fixed escaping for textarea values.

#### 1.8.1 / 2021-10-21
* Refactored Data columns as an option.

#### 1.8 / 2021-10-21
* Added Maintenance mode option.

#### 1.7.2 / 2021-10-19
* renamed plugin

#### 1.7.1 / 2021-03-18
* updated readme, consolidated changelog

#### 1.7 / 2021-03-17
* Added User ID column and Last Login column to Users table.

#### 1.6 / 2020-08-13
* Added plugin icon

#### 1.5 / 2020-08-13
* Added changelog and plugin banner

#### 1.3 / 2020-07-14
* Added ID column

#### 1.0 / 2018-10-25
* Initial commit
